#!/usr/bin/python3
"""
                         celestia-galmon-global
    Retrieve Galmon global system overview data.

    Copyright (C) 2022, Jeff Moe, jebba.
    Author: Jeff Moe

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

# Usage:
# celestia-galmon-global
# Example:
# ./celestia-galmon-global

import pandas as pd

#url='https://galmon.eu/global.json'
url='data/global.json'

galmon_global = pd.read_json(url, orient='index')

beidou_sigs=(galmon_global[0]["beidou-sigs"])
beidou_svs=(galmon_global[0]["beidou-svs"])
beidou_utc_offset_ns=(galmon_global[0]["beidou-utc-offset-ns"])
galileo_sigs=(galmon_global[0]["galileo-sigs"])
galileo_svs=(galmon_global[0]["galileo-svs"])
glonass_sigs=(galmon_global[0]["glonass-sigs"])
glonass_svs=(galmon_global[0]["glonass-svs"])
glonass_utc_offset_ns=(galmon_global[0]["glonass-utc-offset-ns"])
glonass_gps_offset_ns=(galmon_global[0]["glonass-gps-offset-ns"])
gps_sigs=(galmon_global[0]["gps-sigs"])
gps_svs=(galmon_global[0]["gps-svs"])
gps_utc_offset_ns=(galmon_global[0]["gps-utc-offset-ns"])
gst_gps_offset_ns=(galmon_global[0]["gst-gps-offset-ns"])
gst_utc_offset_ns=(galmon_global[0]["gst-utc-offset-ns"])
last_seen=(galmon_global[0]["last-seen"])
leap_seconds=(galmon_global[0]["leap-seconds"])
total_live_receivers=(galmon_global[0]["total-live-receivers"])
total_live_signals=(galmon_global[0]["total-live-signals"])
total_live_svs=(galmon_global[0]["total-live-svs"])

print('beidou-sigs:', beidou_sigs)
print('beidou-svs:', beidou_svs)
print('beidou-utc-offset-ns:', beidou_utc_offset_ns)
print('galileo-sigs:', galileo_sigs)
print('galileo-svs:', galileo_svs)
print('glonass-gps-offset-ns:', glonass_gps_offset_ns)
print('glonass-sigs:', glonass_sigs)
print('glonass-svs:', glonass_svs)
print('glonass-utc-offset-ns:', glonass_utc_offset_ns)
print('gps-sigs:', gps_sigs)
print('gps-svs:', gps_svs)
print('gps-utc-offset-ns:', gps_utc_offset_ns)
print('gst-gps-offset-ns:', gst_gps_offset_ns)
print('gst-utc-offset-ns:', gst_utc_offset_ns)
print('last-seen:', last_seen)
print('leap-seconds:', leap_seconds)
print('total-live-receivers:', total_live_receivers)
print('total-live-signals:', total_live_signals)
print('total-live-svs:', total_live_svs)

